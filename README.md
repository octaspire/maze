[![Build Status](https://travis-ci.org/octaspire/maze.svg?branch=master)](https://travis-ci.org/octaspire/maze) [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://choosealicense.com/licenses/apache-2.0/)


Game written in programming languages Dern and standard C99

[https://octaspire.io/maze](https://octaspire.io/maze)

